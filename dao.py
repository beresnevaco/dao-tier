import mysql.connector as mysql
import json
from datetime import datetime
import numbers
import sys
from time import time

#Code approved and comments left in as they make code readable.

#HOST = "192.168.99.100"
#USER = "root"
#DATABASE = "db_7"
#DATETIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"
#PASSWORD = "%Av8Xa3KJX9apF95%pr^^RSGxsMhDJ#W"

# HOST = "mysql"
# USER = "userOY4"
# DATABASE = "beresneva"
# DATETIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"
# PASSWORD = "uAuHeBupapNo18vW"

HOST = "192.168.99.100"
USER = "root"
DATABASE = "db_7"
DATETIME_FORMAT = "%d-%b-%Y (%H:%M:%S.%f)"
PASSWORD = "%Av8Xa3KJX9apF95%pr^^RSGxsMhDJ#W"

class sql_scripts:
    
    def __get_db(self):
        db = mysql.connect(
            host=HOST,
            user=USER,
            passwd=PASSWORD,
            database=DATABASE
        )
        return db

    def __get_instrument_id_by_name(self, instrument_name, cursor):
        query_for_instrument_id = "SELECT instrument_id " \
                                  "FROM instruments " \
                                  "WHERE instrument_name = %s"
        cursor.execute(query_for_instrument_id, (instrument_name,))
        try:
            ((result,),) = cursor.fetchall()
        except:
            raise ValueError()

        return result

    def __get_counter_party_id_by_name(self, counter_party_name, cursor):
        while True:
            query_for_counter_party_id = "select counter_party_id from counter_parties where counter_party_name = %s"
            cursor.execute(query_for_counter_party_id, (counter_party_name,))

            try:
                ((result,),) = cursor.fetchall()
                break
            except:
                query_for_new_counter_party = "insert into counter_parties (counter_party_name) values (%s)"
                cursor.execute(query_for_new_counter_party, (counter_party_name,))

        return result

    def __get_type_id_by_name(self, type_name, cursor):
        query_for_type_id = "select type_id from types where type_name = %s"
        cursor.execute(query_for_type_id, (type_name,))
        try:
            ((result,),) = cursor.fetchall()
        except:
            raise ValueError()

        return result

    def __convert_cursor_results_to_json_format(self, row_headers, results):
        json_data = []
        for result in results:
            temp_result = []
            for field in result:
                if isinstance(field, numbers.Number):
                    field = float(field)
                if isinstance(field, datetime):
                    field = field.strftime(DATETIME_FORMAT)
                temp_result.append(field)
            json_data.append(dict(zip(row_headers, temp_result)))
        return json.dumps(json_data)

    def __mask_for_select_query_with_json_output(self, query, values):
        db = self.__get_db()
        cursor = db.cursor()

        try:
            cursor.execute(query, values)
        except:
            print("errorrrr")

        row_headers = [x[0] for x in cursor.description]
        results = cursor.fetchall()
        cursor.close()
        return self.__convert_cursor_results_to_json_format(row_headers, results)

    def __mask_for_select_query_with_single_number_result(self, query, values):
        db = self.__get_db()
        cursor = db.cursor()

        try:
            cursor.execute(query, values)
        except:
            print("errorrrr")

        result = 0
        try:
            ((result,),) = cursor.fetchall()
        except ValueError as e:
            raise e
        cursor.close()

        if result is None:
            raise ValueError()

        return result

    def __is_deal_duplicated(self, values):

        query = "SELECT count(*) " \
                "FROM deals " \
                "WHERE instrument_id = %s " \
                "AND counter_party_id = %s " \
                "AND price = %s " \
                "AND type_id = %s " \
                "AND quantity = %s " \
                "AND time = %s"

        return int(self.__mask_for_select_query_with_single_number_result(query, values)) > 0

    # INSERT queries
    def insert_deals_to_mysql_database_from_json(self, json_file):
        with open(json_file) as json_file:
            data = json.load(json_file)
            for one_object in data:
                self.insert_deals_to_mysql_database_from_one_json_object(one_object)

    def insert_deals_to_mysql_database_from_one_json_object(self, json_object):
        db = self.__get_db()
        cursor = db.cursor()

        instrument_name = json_object['instrumentName']
        counter_party_name = json_object['cpty']
        price = json_object['price']
        type_name = json_object['type']
        quantity = json_object['quantity']
        time = json_object['time']

        instrument_id = self.__get_instrument_id_by_name(instrument_name, cursor)
        counter_party_id = self.__get_counter_party_id_by_name(counter_party_name, cursor)
        type_id = self.__get_type_id_by_name(type_name, cursor)
        datetime_in_a_good_format = datetime.strptime(time, DATETIME_FORMAT)
        query = "insert into deals (instrument_id, counter_party_id, price, type_id, quantity, time) " \
                "values(%s, %s, %s, %s, %s, %s)"
        values = (instrument_id, counter_party_id, price, type_id, quantity, datetime_in_a_good_format)
        try:
            if self.__is_deal_duplicated(values):
                #print('Duplicate is found! Oooops')
                #print(values)
                return
            else:
                #print('Added')
                a = 5
            cursor.execute(query, values)
            db.commit()
        except Exception as e:
            print("Unexpected error:", sys.exc_info()[0])
            db.rollback()

        cursor.close()

    # Simple SELECT queries
    def get_all_user_info_by_login(self, user_login):
        query = "SELECT * " \
                "FROM users " \
                "WHERE user_login = %s;"
        values = (user_login,)

        return self.__mask_for_select_query_with_json_output(query, values)

    def get_all_instruments(self):
        query = "SELECT instrument_name " \
                "FROM instruments;"
        values = ()

        return self.__mask_for_select_query_with_json_output(query, values)

    def get_all_instruments_for_time_interval(self, start, end):
        query = "SELECT distinct(instrument_name) " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "WHERE d.time >= %s AND d.time <= %s;"
        values = (start, end)

        return self.__mask_for_select_query_with_json_output(query, values)

    def get_all_counter_parties(self):
        query = "SELECT counter_party_name " \
                "FROM counter_parties;"
        values = ()

        return self.__mask_for_select_query_with_json_output(query, values)

    def select_deals_from_time_interval(self, start, end):
        query = "SELECT r.deal_id, l.instrument_name, c.counter_party_name, r.price, t.type_name, " \
                "r.quantity, r.time FROM instruments l LEFT JOIN deals r using(instrument_id) LEFT JOIN " \
                "types t using(type_id) LEFT JOIN counter_parties c using(counter_party_id) " \
                "where r.time >= %s and r.time <= %s order by r.time;"
        values = (start, end)
        return self.__mask_for_select_query_with_json_output(query, values)

    def select_deals_for_given_instrument_from_time_interval(self, instrument, start, end):
        query = "SELECT r.deal_id, l.instrument_name, c.counter_party_name, r.price, t.type_name, " \
                "r.quantity, r.time FROM instruments l LEFT JOIN deals r using(instrument_id) LEFT JOIN " \
                "types t using(type_id) LEFT JOIN counter_parties c using(counter_party_id) " \
                "where l.instrument_name = %s and r.time >= %s and r.time <= %s;"
        values = (instrument, start, end)
        return self.__mask_for_select_query_with_json_output(query, values)

    def select_average_price_for_each_instrument(self, type_name):
        if type_name not in ('B', 'S'):
            raise ValueError()

        query = "SELECT l.instrument_name, AVG(r.price) as average_price " \
                    "FROM instruments l " \
                    "LEFT JOIN deals r using(instrument_id) " \
                    "LEFT JOIN types t using(type_id) " \
                    "WHERE t.type_name = %s " \
                    "GROUP BY l.instrument_name;"
        values = (type_name,)

        return self.__mask_for_select_query_with_json_output(query, values)

    def select_average_price_for_each_instrument_for_time_interval(self, type_name, start, end):
        if type_name not in ('B', 'S'):
            raise ValueError()

        query = "SELECT l.instrument_name, AVG(r.price) as average_price " \
                    "FROM instruments l " \
                    "LEFT JOIN deals r using(instrument_id) " \
                    "LEFT JOIN types t using(type_id) " \
                    "WHERE t.type_name = %s AND " \
                    "r.time >= %s AND r.time <= %s " \
                    "GROUP BY l.instrument_name;"
        values = (type_name, start, end)

        return self.__mask_for_select_query_with_json_output(query, values)

    def select_all_data(self):
        query = "SELECT r.deal_id, l.instrument_name, c.counter_party_name, r.price, t.type_name, " \
                "r.quantity, r.time FROM instruments l LEFT JOIN deals r using(instrument_id) LEFT JOIN " \
                "types t using(type_id) LEFT JOIN counter_parties c using(counter_party_id);"
        values = ()

        return self.__mask_for_select_query_with_json_output(query, values)

    # SELECT queries for realized and effective profit/loss

    def __get_current_price_for_BUY_or_SELL_for_given_instrument(self, type_name, instrument_name, start, end):
        query = "SELECT d.price " \
                "FROM deals d " \
                "WHERE d.time = (" \
                "SELECT MAX(time) as sell_max_time " \
                "FROM deals d " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "WHERE t.type_name = %s " \
                "AND i.instrument_name = %s " \
                "AND d.time >= %s " \
                "AND d.time <= %s);"
        values = (type_name, instrument_name, start, end)

        res = self.__mask_for_select_query_with_single_number_result(query, values)
        if not isinstance(res, numbers.Number):
            raise ValueError('can\'t find current price')

        return float(res)

    def __select_all_BUY_or_SELL_deals_for_counter_party_for_time_interval(self, counter_party_name, type_name, start, end):
        query = "SELECT price, quantity, time, counter_party_name " \
                "FROM deals d " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN counter_parties c using(counter_party_id) " \
                "WHERE c.counter_party_name = %s " \
                "AND t.type_name = %s " \
                "AND d.time >= %s " \
                "AND d.time <= %s" \
                "ORDER BY d.time;"
        values = (counter_party_name, type_name, start, end)

        return self.__mask_for_select_query_with_json_output(query, values)

    def __select_all_BUY_or_SELL_deals_for_instrument_for_counter_party_for_time_interval(self, instrument_name, counter_party_name, type_name, start, end):
        query = "SELECT price, quantity, time " \
                "FROM deals d " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN counter_parties c using(counter_party_id) " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "WHERE i.instrument_name = %s " \
                "AND c.counter_party_name = %s " \
                "AND t.type_name = %s " \
                "AND d.time >= %s " \
                "AND d.time <= %s" \
                "ORDER BY d.time;"
        values = (instrument_name, counter_party_name, type_name, start, end)

        return self.__mask_for_select_query_with_json_output(query, values)

    def __get_number_of_deals(self, deals):
        num = 0
        data = json.loads(deals)
        for trade in data:
            num += trade["quantity"]
        return num

    def __get_money_for_given_m_deals(self, deals, m):
        cur_quantity = 0
        money = 0
        data = json.loads(deals)
        for trade in data:
            cur_quantity += trade["quantity"]
            if cur_quantity <= m:
                money += trade["quantity"] * trade["price"]
            else:
                money += (trade["quantity"] - (cur_quantity - m)) * trade["price"]
                break
        return money

    def __get_realized_profit_for_counter_party_for_time_interval_BY_SQL(self, counter_party_name, start, end):

        bought_deals = self.__select_all_BUY_or_SELL_deals_for_counter_party_for_time_interval(counter_party_name, 'B',
                                                                                               start, end)
        sold_deals = self.__select_all_BUY_or_SELL_deals_for_counter_party_for_time_interval(counter_party_name, 'S',
                                                                                             start, end)

        m = min(self.__get_number_of_deals(bought_deals), self.__get_number_of_deals(sold_deals))

        try:
            result_for_SOLD = self.__get_smth(m, 'S', counter_party_name, start, end)
            result_for_BOUGHT = self.__get_smth(m, 'B', counter_party_name, start, end)
        except:
            return "No data"

        if result_for_BOUGHT is None:
            return "Nothing was bought"

        if result_for_SOLD is None:
            result_for_SOLD = 0

        final_result = result_for_SOLD - result_for_BOUGHT

        return float(final_result)

    def __get_realized_profits_for_all_counter_parties_for_time_interval_BY_SQL(self, start, end):
        counter_parties = self.get_all_counter_parties()
        data = json.loads(counter_parties)
        res_list = []
        for counter_party in data:
            counter_party_name = counter_party["counter_party_name"]
            res = self.__get_realized_profit_for_counter_party_for_time_interval_BY_SQL(counter_party_name, start, end)
            res_list.append((counter_party_name, res))

        return self.__convert_cursor_results_to_json_format(["counter_party_name", "realized_profit"], res_list)


    def __NEWget_realized_and_effective_profit_for_counter_party_for_time_interval(self, counter_party_name, start, end):

        query = "SELECT SUM(AAA) FROM (" \
                "SELECT *, " \
                "CASE " \
                "WHEN num_of_sold > num_of_bought THEN (num_of_bought - num_of_sold) * current_buy_price " \
                "ELSE (num_of_bought - num_of_sold) * current_sell_price END as AAA " \
                "FROM (" \
                "SELECT instrument_name, IFNULL(sell_quantity, 0) as num_of_sold, IFNULL(buy_quantity, 0) as num_of_bought FROM " \
                "(SELECT i.instrument_name, SUM(d.quantity) as sell_quantity " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN counter_parties c USING(counter_party_id) " \
                "WHERE t.type_name = 'S' AND c.counter_party_name = %s AND d.time >= %s AND d.time <= %s " \
                "GROUP BY i.instrument_name) as a " \
                "LEFT JOIN " \
                "(SELECT i.instrument_name, SUM(d.quantity) as buy_quantity " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN counter_parties c USING(counter_party_id) " \
                "WHERE t.type_name = 'B' AND c.counter_party_name = %s AND d.time >= %s AND d.time <= %s " \
                "GROUP BY i.instrument_name" \
                ") as b " \
                "USING(instrument_name) " \
                "UNION " \
                "SELECT instrument_name, IFNULL(sell_quantity, 0) as num_of_sold, IFNULL(buy_quantity, 0) as num_of_bought FROM " \
                "(" \
                "SELECT i.instrument_name, SUM(d.quantity) as sell_quantity " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN counter_parties c USING(counter_party_id) " \
                "WHERE t.type_name = 'S' AND c.counter_party_name = %s AND d.time >= %s AND d.time <= %s " \
                "GROUP BY i.instrument_name" \
                ") as x " \
                "RIGHT JOIN " \
                "(" \
                "SELECT i.instrument_name, SUM(d.quantity) as buy_quantity " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN counter_parties c USING(counter_party_id) " \
                "WHERE t.type_name = 'B' AND c.counter_party_name = %s AND d.time >= %s AND d.time <= %s " \
                "GROUP BY i.instrument_name) as y " \
                "USING(instrument_name)) as abc " \
                "LEFT JOIN " \
                "(" \
                "SELECT instrument_name, IFNULL(current_sell_price, 0) as current_sell_price, IFNULL(current_buy_price, 0) as current_buy_price " \
                "FROM (" \
                "SELECT price as current_sell_price, xxx.instrument_name " \
                "FROM ( " \
                "SELECT * " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id)" \
                ") as xxx " \
                "INNER JOIN " \
                "( " \
                "SELECT instrument_name, MAX(time) AS MaxDateTime " \
                "FROM deals d " \
                "LEFT JOIN types t1 USING(type_id) " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "WHERE t1.type_name = 'S' AND d.time >= %s AND d.time <= %s " \
                "GROUP BY instrument_name " \
                ") as yyy ON xxx.instrument_name = yyy.instrument_name AND xxx.time = yyy.MaxDateTime) as a " \
                "LEFT JOIN(SELECT price as current_buy_price, xxx.instrument_name " \
                "FROM (" \
                "SELECT * " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id)) as xxx " \
                "INNER JOIN (" \
                "SELECT instrument_name, MAX(time) AS MaxDateTime " \
                "FROM deals d " \
                "LEFT JOIN types t1 USING(type_id) " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "WHERE t1.type_name = 'B' AND d.time >= %s AND d.time <= %s " \
                "GROUP BY instrument_name) as yyy " \
                "ON xxx.instrument_name = yyy.instrument_name AND xxx.time = yyy.MaxDateTime) as b " \
                "USING (instrument_name) " \
                "UNION " \
                "SELECT instrument_name, IFNULL(current_sell_price, 0) as current_sell_price, IFNULL(current_buy_price, 0) as current_buy_price " \
                "FROM ( " \
                "SELECT price as current_sell_price, xxx.instrument_name " \
                "FROM (" \
                "SELECT * " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id)) as xxx " \
                "INNER JOIN (SELECT instrument_name, MAX(time) AS MaxDateTime FROM deals d " \
                "LEFT JOIN types t1 USING(type_id) " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "WHERE t1.type_name = 'S' AND d.time >= %s AND d.time <= %s " \
                "GROUP BY instrument_name) as yyy " \
                "ON xxx.instrument_name = yyy.instrument_name AND xxx.time = yyy.MaxDateTime) as c " \
                "RIGHT JOIN (" \
                "SELECT price as current_buy_price, xxx.instrument_name " \
                "FROM ( " \
                "SELECT * " \
                "FROM deals d " \
                "LEFT JOIN instruments i USING(instrument_id)) as xxx " \
                "INNER JOIN(" \
                "SELECT instrument_name, MAX(time) AS MaxDateTime " \
                "FROM deals d " \
                "LEFT JOIN types t1 USING(type_id) " \
                "LEFT JOIN instruments i USING(instrument_id) " \
                "WHERE t1.type_name = 'B' AND d.time >= %s AND d.time <= %s " \
                "GROUP BY instrument_name) as yyy " \
                "ON " \
                "xxx.instrument_name = yyy.instrument_name " \
                "AND xxx.time = yyy.MaxDateTime) as d " \
                "USING (instrument_name)) as cba " \
                "USING(instrument_name)) as aaaaaa;"

        values = (counter_party_name, start, end,
                  counter_party_name, start, end,
                  counter_party_name, start, end,
                  counter_party_name, start, end,
                  start, end,
                  start, end,
                  start, end,
                  start, end
                  )

        realized_profit = self.__get_realized_profit_for_counter_party_for_time_interval_BY_SQL(counter_party_name,
                                                                                                start,
                                                                                                end)

        if not isinstance(realized_profit, numbers.Number):
            #return "Realized profit is None", "That's why effective profit can't be calculated"
            realized_profit = 0

        try:
            total = float(self.__mask_for_select_query_with_single_number_result(query, values))
        except:
            return realized_profit, "No data found"



        return [realized_profit, total + realized_profit]

    def get_all_profits_for_all_counter_parties_for_time_interval_BY_SQL(self, start, end):
        counter_parties = self.get_all_counter_parties()
        data = json.loads(counter_parties)
        res_list = []
        for counter_party in data:
            counter_party_name = counter_party["counter_party_name"]
            realized, effective = self.__NEWget_realized_and_effective_profit_for_counter_party_for_time_interval(counter_party_name, start, end)
            res_list.append((counter_party_name, realized, effective))

        return self.__convert_cursor_results_to_json_format(["counter_party_name", "realized_profit", "effective_profit"], res_list)

    def __get_smth(self, m, type_name, counter_party_name, start, end):
        query = "SELECT SUM(money) " \
                "FROM ( " \
                "SELECT *, (t4.price * ostatok) as money " \
                "FROM ( " \
                "SELECT t3.price, t3.quantity, " \
                "CASE WHEN cumulative_sum < %s THEN t3.quantity " \
                "ELSE t3.quantity - (cumulative_sum - %s) " \
                "END as ostatok " \
                "FROM " \
                "( " \
                "SELECT *, " \
                "( SELECT SUM(t1.quantity) " \
                "FROM " \
                "( " \
                "SELECT * " \
                "FROM deals d " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN counter_parties c USING(counter_party_id) " \
                "WHERE t.type_name = %s " \
                "AND c.counter_party_name = %s " \
                "AND d.time >= %s " \
                "AND d.time <= %s " \
                "ORDER BY d.time " \
                ") as t1 " \
                "WHERE t1.deal_id <= t2.deal_id " \
                ") AS cumulative_sum " \
                "FROM " \
                "( " \
                "SELECT * " \
                "FROM deals d " \
                "LEFT JOIN types t USING(type_id) " \
                "LEFT JOIN counter_parties c USING(counter_party_id) " \
                "WHERE t.type_name = %s " \
                "AND c.counter_party_name = %s " \
                "AND d.time >= %s " \
                "AND d.time <= %s " \
                "ORDER BY d.time " \
                ") as t2 " \
                "ORDER BY t2.deal_id " \
                ") as t3 " \
                ") as t4 " \
                ") as t5;"
        values = (m, m, type_name, counter_party_name, start, end, type_name, counter_party_name, start, end)

        return self.__mask_for_select_query_with_single_number_result(query, values)
