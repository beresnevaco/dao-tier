from flask import Flask, Response
from flask_cors import CORS
from dao import sql_scripts
import json
import requests
import datetime

app = Flask(__name__)
CORS(app)

#DataGen_Tier_ADDRESS='http://192.168.99.100:3005/'
DataGen_Tier_ADDRESS='https://tinyurl.com/yygfwxru'


@app.route('/')
def refer_to_accepted_format():
    
    return Response('''

        Supported Paths: \n 
        \n 
        /dao/store/datastream \n 
        /dao/user/<user_login> \n 
        /dao/instrument/list \n 
        /dao/instrument/<instrument>/<start_date>/<end_date> \n
        /dao/counterparty/stats/<start_date>/<end_date>
        ''', mimetype="text/plainText")

@app.route('/dao/instrument/list')
def get_all_instruments():
   
    instruments = sql_scripts().get_all_instruments()

    return Response(instruments, mimetype="application/json")

@app.route('/dao/user/<user_login>')
def get_user(user_login):
   
    print(user_login)

    user = sql_scripts().get_all_user_info_by_login(user_login)

    return Response(user, mimetype="application/json")
    

@app.route('/dao/store/datastream')
def collect_data_from_datastream():

    r = requests.get(DataGen_Tier_ADDRESS, stream=True)

    data_batch = []

    for line in r.iter_lines():

        json_data = None

        if line:

            json_data = json.loads(line[5:])

            if len(data_batch) < 20 and json_data is not None:
                
                data_batch.append(json_data)
            
            else:

                _write_data_batch_to_database(data_batch)
                data_batch = []
                
def _write_data_batch_to_database(data_batch):

    for json_data in data_batch:

        sql_scripts().insert_deals_to_mysql_database_from_one_json_object(json_data)

@app.route('/dao/instrument/<instrument>/<start_date>/<end_date>')
def get_all_deals_for_one_instrument(instrument, start_date, end_date):

    start_date = datetime.datetime.fromtimestamp(int(start_date)).strftime("%Y-%m-%d %H:%M:%S")
    end_date = datetime.datetime.fromtimestamp(int(end_date)).strftime("%Y-%m-%d %H:%M:%S")
  
    return sql_scripts().select_deals_for_given_instrument_from_time_interval(instrument, start_date, end_date)

@app.route('/dao/counterparty/stats/<start_date>/<end_date>')
def get_effective_profits(start_date, end_date):

    start_date = datetime.datetime.fromtimestamp(int(start_date)).strftime("%Y-%m-%d %H:%M:%S")
    end_date = datetime.datetime.fromtimestamp(int(end_date)).strftime("%Y-%m-%d %H:%M:%S")

    print(start_date)
    print(end_date)
  
    stats = sql_scripts().get_all_profits_for_all_counter_parties_for_time_interval_BY_SQL(start_date, end_date)

    return Response(stats, mimetype="application/json")


if __name__ == "__main__":

    # Local
    # app.run("localhost", port=3000, debug=True)

    # Docker:
    app.run("0.0.0.0", port=3000, debug=True)

    # OpenShift:
    # app.run("0.0.0.0", port=8080, debug=True)
  
    # 1565784156